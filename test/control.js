function logStatus(_requestParams, response, _context, _ee, next) {
  console.log(response.statusCode)
  return next() // MUST be called for the scenario to continue
}

module.exports = { logStatus }
