import assert from 'assert'
import { calculateBonuses } from '../src/bonus-system'

describe('Bonus system tests', () => {
  console.group('Bonus system tests')

  /**
   * Wrong program
   */

  test('Invalid program', (done) => {
    assert.strictEqual(
      calculateBonuses('*', 1000),
      0
    )
    done()
  })

  /**
   * Standard program
   */

  test('Standard0', (done) => {
    assert.strictEqual(
      calculateBonuses('Standard', 0),
      0.05 * 1
    )
    done()
  })

  test('Standard10000', (done) => {
    assert.strictEqual(
      calculateBonuses('Standard', 10000),
      0.05 * 1.5
    )
    done()
  })

  test('Standard50000', (done) => {
    assert.strictEqual(
      calculateBonuses('Standard', 50000),
      0.05 * 2
    )
    done()
  })

  test('Standard100000', (done) => {
    assert.strictEqual(
      calculateBonuses('Standard', 100000),
      0.05 * 2.5
    )
    done()
  })

  /**
   * Premium program
   */

  test('Premium0', (done) => {
    assert.strictEqual(
      calculateBonuses('Premium', 0),
      0.1 * 1
    )
    done()
  })

  test('Premium10000', (done) => {
    assert.strictEqual(
      calculateBonuses('Premium', 10000),
      0.1 * 1.5
    )
    done()
  })

  test('Premium50000', (done) => {
    assert.strictEqual(
      calculateBonuses('Premium', 50000),
      0.1 * 2
    )
    done()
  })

  test('Premium100000', (done) => {
    assert.strictEqual(
      calculateBonuses('Premium', 100000),
      0.1 * 2.5
    )
    done()
  })

  /**
   * Diamond program
   */

  test('Diamond0', (done) => {
    assert.strictEqual(
      calculateBonuses('Diamond', 0),
      0.2 * 1
    )
    done()
  })

  test('Diamond10000', (done) => {
    assert.strictEqual(
      calculateBonuses('Diamond', 10000),
      0.2 * 1.5
    )
    done()
  })

  test('Diamond50000', (done) => {
    assert.strictEqual(
      calculateBonuses('Diamond', 50000),
      0.2 * 2
    )
    done()
  })

  test('Diamond100000', (done) => {
    assert.strictEqual(
      calculateBonuses('Diamond', 100000),
      0.2 * 2.5
    )
    done()
  })

  console.groupEnd('Bonus system tests')
})
